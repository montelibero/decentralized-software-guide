# Чек-лист для добавления вещей в [Основной список](README.md#Основной список)
1. Проверить, что тем, что вы добавляете, действительно можно воспользоваться, не обладая специализированными навыками. Описать или добавить ссылку на качественное описание того, как этим начать пользоваться.
2. Проверить, какие из ожидаемых от подобной программы/сервиса функций работают, а какие нет. Записать это. Описать дополнительную функциональность, если она есть.
3. Поставить ссылку на каждый децентрализованный проект; по возможности на страницу, которая больше всего приближает человека к началу использования (страница скачивания, список __стабильно работающих__ публичных серверов и т. д.)
4. Проверить совпадения написания названия с авторским.
