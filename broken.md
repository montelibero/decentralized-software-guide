## Файлообменники, облачные хранилища, синхронизация данных
### [MuWire](https://github.com/zlatinb/muwire)
Проект закрыт. Приложение больше нельзя скачать, но можно собрать из исходников.
### Онлайн-офис
### [hyperpad](https://github.com/hackergrrl/hyperpad)
Судя по репозиториям всё заброшено.
### [peer-pad](https://github.com/peer-base/peer-pad)
Судя по репозиториям всё заброшено.
## Мессенджеры
### [Jami](https://jami.net)
Сообщения и звонки зачастую не доходят до адресата.
